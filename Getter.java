import java.util.Scanner;

/**
 * Created by Djahid on 24.03.2016.
 */
public class Getter {
    public String getName(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Hey what is your name?");
       String name = sc.nextLine();
        return name;

    }
    public float getKG(String name){
        System.out.println(name +", enter your weight in kilograms, please (Example: 80.0)");
        Scanner sc = new Scanner(System.in);
        String kg = sc.nextLine();
        float asFloat = Float.parseFloat(kg);
        return asFloat;

    }

    public float getMeters(String name){
        Scanner sc = new Scanner(System.in);
        System.out.println("Okay"+ name + ". Now enter your height in meters (Example: 1.80)");
        String meters = sc.nextLine();
        Float asFloat = Float.parseFloat(meters);
        return asFloat;

    }

}
